eclipse:
	@gradle eclipse

javadoc:
	@gradle assemble javadoc

run:
	@gradle assemble run

test:
	@gradle assemble test

.PHONY: clean
clean:
	@gradle clean cleanEclipse
	@rm -rf bin

pdf:
	@pdflatex src/main/latex/relatorio.tex

r:
	@R --vanilla < src/main/r/boxplot.r
