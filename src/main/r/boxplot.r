
mlp <- read.csv(file="src/main/latex/mlp.csv",head=TRUE,sep=",");
attr <- levels(mlp$attributes);
learning <- c(0.1,0.3,0.8);
momentum <- c(0.2,0.6,0.8);
node <- levels(mlp$nodes);
jpeg("attributes.jpg");
boxplot(mlp$fitness~mlp$attributes, main="Fitness per Attributes",
        xlab="Attributes", ylab="Fitness");
dev.off();
jpeg("learning.jpg");
boxplot(mlp$fitness~mlp$learning, main="Fitness per Learning",
        xlab="Learning", ylab="Fitness");
dev.off();
jpeg("momentum.jpg");
boxplot(mlp$fitness~mlp$momentum,main="Fitness per Momentum",
        xlab="Momentum", ylab="Fitness");
dev.off();
jpeg("nodes.jpg");
boxplot(mlp$fitness~mlp$nodes,main="Fitness per Nodes",
        xlab="Nodes", ylab="Fitness");
dev.off();

