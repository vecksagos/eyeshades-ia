package model;

import java.util.ArrayList;
import java.util.List;

import weka.core.Attribute;
import weka.core.DenseInstance;
import weka.core.Instance;
import weka.core.Instances;

public class Eye {
	static public Instance getInstance(List<Double> attrs) {
		ArrayList<Attribute> v = new ArrayList<Attribute>(15);
		
		List<String>classes = new ArrayList<String>(2);
		classes.add("Baixo");
		classes.add("Alto");
		
		v.add(new Attribute("ID"));
		v.add(new Attribute("class", classes));
		v.add(new Attribute("A"));
		v.add(new Attribute("B"));
		v.add(new Attribute("C"));
		v.add(new Attribute("D"));
		v.add(new Attribute("E"));
		v.add(new Attribute("F"));
		v.add(new Attribute("G"));
		v.add(new Attribute("H"));
		v.add(new Attribute("I"));
		v.add(new Attribute("J"));
		v.add(new Attribute("L"));
		v.add(new Attribute("M"));
		v.add(new Attribute("N"));
		
		Instances instances = new Instances("Test", v, 0);
		instances.setClassIndex(1);
		
		Instance instance = new DenseInstance(v.size());
		instance.setDataset(instances);
		for (int i = 0; i < attrs.size(); ++i) {
			instance.setValue(instance.attribute(i + 2), attrs.get(i));
		}
		
		return instance;
	}
}
