package ga.individual;

public class AttrIndividual extends AbsIndividual {
		
	@Override
	public String toString() {
		int setting = gene >> 3 & 0x1FFF;
		String s = "";
		for (int i = 0; i < 13; ++i) {
			if (((setting >> i) & 0x1) == 0x1) {
				int k = 0;
				if (('a' + i) >= 'k')
					k = 1;
				s += (char) ('a' + (i + k));
				if (i != 12)
					s += ",";
			}
		}
		return super.toString() + " Attributes: " + s;
	}
	
	public String attr() {
		int setting = gene >> 3 & 0x1FFF;
		String s = "";
		for (int i = 0; i < 13; ++i) {
			if (((setting >> i) & 0x1) == 0x1) {
				int k = 0;
				if (('a' + i) >= 'k')
					k = 1;
				s += (char) ('A' + (i + k));
			}
		}
		
		return s;
	}
}
