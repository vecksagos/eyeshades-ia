package ga.individual;

import java.util.Comparator;

import ga.Ga;
import ga.Population;
import ga.factory.Factory;
import ga.factory.IndividualFactory;
import weka.classifiers.meta.FilteredClassifier;

public interface Individual {
	static public Individual useGa() {
		Factory<Individual> f = new IndividualFactory();
		int rounds = 10;
		Population population = new Population(25, f);

		int generation = 1;
		int generationBest = 1;
		Ga ga = new Ga(f);
		Individual best = population.fittest();
		Individual currentBest;
		for (int i = 0; i < rounds; ++i, ++generation) {
			currentBest = population.fittest();
			System.out.println("Generation: " + generation + " Fittest: " + currentBest);
			if (best.fitness() < currentBest.fitness()) {
				best = currentBest;
				generationBest = i + 1;
			}
			population = ga.evolve(population);
		}

		System.out.println("Best\ngeneration: " + generationBest + "\nIndividual: " + best);
		return best;
	}

	static Comparator<Individual> comparator = new Comparator<Individual>() {
		public int compare(Individual i1, Individual i2) {
			return i1.compareTo(i2);
		}
	};

	boolean getGene(int i);

	void setGene(int i, boolean value);

	double fitness();

	int compareTo(Individual individual);

	FilteredClassifier classifier();

	void generate(String value);
}
