package ga.individual;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.Random;

import ga.Ga;
import weka.classifiers.Classifier;
import weka.classifiers.Evaluation;
import weka.classifiers.functions.MultilayerPerceptron;
import weka.classifiers.lazy.IBk;
import weka.classifiers.meta.FilteredClassifier;
import weka.classifiers.trees.J48;
import weka.filters.unsupervised.attribute.Remove;

public abstract class AbsIndividual implements Individual {
	protected short gene = 0;
	private double fitness = -1;
	static private List<Classifier> classifiers = null;

	@Override
	public boolean getGene(int i) {
		return ((gene >> i) & 0x1) == 0x1 ? true : false;
	}

	@Override
	public void setGene(int i, boolean value) {
		--i;
		if (i <= 0)
			i = 0;

		short s = 0;
		if (value)
			s = 1;

		gene ^= (-s ^ gene) & (1 << i);
	}

	@Override
	public double fitness() {
		if (fitness >= 0)
			return fitness;

		try {
			FilteredClassifier fc = classifier();
			fc.buildClassifier(Ga.getData());

			Evaluation eval = new Evaluation(Ga.getData());
			eval.crossValidateModel(fc, Ga.getData(), 100, new Random(1));
			fitness = eval.recall(0) + (eval.recall(1) * 1.5);
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return fitness;
	}

	@Override
	public int compareTo(Individual individual) {
		double res = fitness() - individual.fitness();
		if (res == 0)
			return 0;

		return res > 0 ? -1 : 1;
	}

	@Override
	public String toString() {
		String s = "";

		for (int i = 0; i < 16; ++i)
			s = ((gene >> i) & 0x1) == 0x1 ? "1" + s : "0" + s;

		return String.valueOf(fitness) + " " + s;
	}

	public FilteredClassifier classifier() {
		int c = gene & 0x7;
		if (classifiers == null) {
			J48 j48 = new J48();
			IBk ibk = new IBk(3);
			MultilayerPerceptron mlp = new MultilayerPerceptron();
			mlp.setHiddenLayers("a");
			classifiers = new ArrayList<Classifier>(Arrays.asList(j48, ibk, mlp));
		}
		Classifier classifier = classifiers.get(c % classifiers.size());

		int setting = gene >> 3 & 0x1FFF;
		Remove rm = new Remove();
		String s = "";
		for (int i = 0; i < 13; ++i) {
			if (((setting >> i) & 0x1) == 0x0)
				s += String.valueOf(i + 2);
			if (i < 12)
				s += ",";
		}
		rm.setAttributeIndices(s);

		FilteredClassifier fc = new FilteredClassifier();
		fc.setFilter(rm);
		fc.setClassifier(classifier);

		return fc;
	}

	@Override
	public void generate(String value) {
		if (!value.equals("")) {
			for (int i = 0; i < 16; ++i)
				gene |= (Short.valueOf(Character.toString(value.charAt(15 - i))) << i);
		} else {
			Random r = new Random();
			for (int i = 0; i < 16; ++i)
				gene |= ((short) r.nextInt(2)) << i;
		}
	}
}
