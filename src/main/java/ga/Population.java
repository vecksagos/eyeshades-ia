package ga;

import java.util.ArrayList;
import java.util.List;

import ga.factory.Factory;
import ga.individual.Individual;

public class Population {
	private List<Individual> individuals;
	
	public Population() {
		individuals = new ArrayList<Individual>();
	}
	
	public Population(int size, Factory<Individual> factory) {
		individuals = new ArrayList<Individual>(size);
		
		for (int i = 0; i < size; ++i) {
			Individual individual = factory.create();
			individual.generate("");
			individuals.add(individual);
		}
	}
	
	public Individual get(int i) {
		return individuals.get(i);
	}
	
	public Individual fittest() {
		individuals.sort(Individual.comparator);
		
		return individuals.get(0);
	}
	
	public void add(Individual i) {
		individuals.add(i);
	}
	
	public int size() {
		return individuals.size();
	}
}
