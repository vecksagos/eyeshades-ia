package ga;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.Random;

import ga.factory.Factory;
import ga.individual.Individual;
import weka.classifiers.meta.FilteredClassifier;
import weka.core.Instance;
import weka.core.Instances;
import weka.core.converters.ConverterUtils.DataSource;
import weka.filters.Filter;
import weka.filters.unsupervised.attribute.Normalize;
import weka.filters.unsupervised.attribute.Remove;

public class Ga {
	
	private Factory<Individual> factory = null;
	final private double uniformRate = 0.5;
	final private double mutationRate = 0.015;
	final private int tournamentSize = 5;
	final private boolean elitism = true;
	static private Instances data = null;
	
	public Ga(Factory<Individual> factory) {
		this.factory = factory;
	}
	
	static public Instances getData() throws Exception {
		if (data == null) {
			data = (new DataSource(ClassLoader.getSystemResource("train.csv").getPath())).getDataSet();
			data.setClassIndex(1);
			List<Filter> list = new ArrayList<Filter>(Arrays.asList(new Remove(), new Normalize()));
			for (int i = 0; i < list.size(); ++i) {
				Filter f = list.get(i);
				if (i == 0)
					((Remove) f).setAttributeIndices("1");
				f.setInputFormat(data);
				for (int j = 0; j < data.numInstances(); ++j)
					f.input(data.instance(j));

				f.batchFinished();
				data = f.getOutputFormat();
				Instance in;
				while ((in = f.output()) != null)
					data.add(in);
			}
		}

		return data;
	}
	
	public Population evolve(Population pop) {
		Population newPop = new Population();
		
		if (elitism)
			newPop.add(pop.fittest());
		
		int elitismOffset = (elitism) ? 1 : 0;
		for (int i = elitismOffset; i < pop.size(); ++i) {
			Individual i1 = tournament(pop);
			Individual i2 = tournament(pop);
			Individual newI = crossover(i1, i2);
			newPop.add(newI);
		}
		
		for (int i = elitismOffset; i < newPop.size(); ++i) {
			mutate(newPop.get(i));
		}
		
		return newPop;
	}
	
	private Individual crossover(Individual i1, Individual i2) {
		Individual newI = factory.create();
		Random r = new Random();
		for (int i = 0; i < 16; ++i)
				newI.setGene(i, (r.nextDouble() <= uniformRate) ? i1.getGene(i) : i2.getGene(i));
		
		return newI;
	}
	
	private void mutate(Individual ind) {
		Random r = new Random();
		for (int i = 0; i < 16; ++i) {
			if (r.nextDouble() <= mutationRate)
				ind.setGene(i, r.nextBoolean());
		}
	}
	
	private Individual tournament(Population pop) {
		Population tournament = new Population();
		Random r = new Random();
		for (int i = 0; i < tournamentSize; ++i)
			tournament.add(pop.get(r.nextInt(pop.size())));
		
		return tournament.fittest();
	}
	
	public static void main(String[] args) throws Exception {
		Individual i = Individual.useGa();
		FilteredClassifier fc = i.classifier();
		fc.buildClassifier(getData());
		System.out.println(fc + "\n" + i.fitness());
	}
}
