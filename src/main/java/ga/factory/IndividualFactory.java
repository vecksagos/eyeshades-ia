package ga.factory;

import ga.individual.AttrIndividual;
import ga.individual.Individual;

public class IndividualFactory implements Factory<Individual> {

	@Override
	public Individual create() {
		return new AttrIndividual();
	}
}
