package ga.factory;

public interface Factory<E> {
	E create();
}
