package lm;

import java.io.BufferedWriter;
import java.io.FileWriter;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.Random;

import ga.Ga;
import ga.individual.AttrIndividual;
import ga.individual.Individual;
import weka.classifiers.Classifier;
import weka.classifiers.Evaluation;
import weka.classifiers.functions.MultilayerPerceptron;
import weka.classifiers.meta.FilteredClassifier;

public class Main {

	public static void main(String[] args) {
		//csv();
		List<String> attr = new ArrayList<String>(
				Arrays.asList("1111111111111010", "0000110101000010", "0000011111110010"));	
		for (int j = 0; j < 3; ++j) {	
			Individual i = new AttrIndividual();
			i.generate(attr.get(j));
			System.out.println(((AttrIndividual)i).attr());
		}
	}

	private void csv() {
		try {
			List<Double> learning = new ArrayList<Double>(Arrays.asList(0.1, 0.3, 0.8));
			List<Double> momentum = new ArrayList<Double>(Arrays.asList(0.2, 0.6, 0.8));
			List<String> node = new ArrayList<String>(Arrays.asList("a", "30", "60"));
			List<String> attr = new ArrayList<String>(
					Arrays.asList("1111111111111010", "0000110101000010", "0000011111110010"));
			BufferedWriter ip = new BufferedWriter(new FileWriter("res"));
			ip.append("Id,Attributes,Learning,Momentum,Nodes,Fitness");
			int count = 1;
			for (int cl = 0; cl < 3; ++cl) {
				Individual ind = new AttrIndividual();
				ind.generate(attr.get(cl));
				FilteredClassifier fc = ind.classifier();
				for (int i = 0; i < 3; ++i) {
					Classifier c = new MultilayerPerceptron();
					((MultilayerPerceptron) c).setLearningRate(learning.get(i));
					for (int j = 0; j < 3; ++j) {
						((MultilayerPerceptron) c).setMomentum(momentum.get(j));
						for (int k = 0; k < 3; ++k) {
						((MultilayerPerceptron) c).setHiddenLayers(node.get(k));
						fc.setClassifier(c);
						fc.buildClassifier(Ga.getData());

						Evaluation eval = new Evaluation(Ga.getData());
						eval.crossValidateModel(fc, Ga.getData(), 100, new Random(1));
						ip.append(count + "," + ((AttrIndividual) ind).attr() + "," + learning.get(i) + ","
								+ momentum.get(j) + "," + node.get(k) + ","
								+ (eval.recall(0) + (1.5 * eval.recall(1))));
                                                ip.newLine();
						}
					}
				}
			}
			ip.close();
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}
}
